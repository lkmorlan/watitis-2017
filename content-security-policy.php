<?php

/**
 * @file
 * Outputs the Content Security Policy headers used by the WCMS profile.
 *
 * This file is stored here for version control purposes. The headers need to be
 * configured as part of the web server configuration.
 */

$items = array(
  "'self'",

  // These are all bad for security, but they are needed for various things such as MathJax.
  "'unsafe-inline'",
  "'unsafe-eval'",
  'data:',
);

$sites = array(
  // University of Waterloo.
  'uwaterloo.ca',
  '*.uwaterloo.ca',
  // Bootstrap.
  'maxcdn.bootstrapcdn.com',
  // Google.
  '*.google.com',
  '*.google-analytics.com',
  '*.googleapis.com',
  '*.gstatic.com',
  '*.googletagmanager.com',
  '*.googleusercontent.com',
  // Google Analytics.
  'stats.g.doubleclick.net',
  // Google's DoubleClick, for MSI's use.
  '6263835.fls.doubleclick.net',
  // CDNjs (Cloudflare)
  'cdnjs.cloudflare.com',
  // Twitter.
  '*.twitter.com',
  '*.twimg.com',
  'twitter-widgets.s3.amazonaws.com',
  // FaceBook.
  '*.facebook.com',
  '*.facebook.net',
  // YouTube.
  '*.youtube.com',
  '*.youtube-nocookie.com',
  // Livestream.
  '*.livestream.com',
  // WebSpellChecker.net.
  '*.webspellchecker.net',
  // MathJax.
  'cdn.mathjax.org',
  // storify.com.
  'storify.com',
  // AddToAny.
  '*.addtoany.com',
  // Vimeo.
  '*.vimeo.com',
  '*.vimeocdn.com',
  // Tint.
  '*.tintup.com',
  // Tint; RT#456523.
  '*.71n7.com',
  'd36hc0p18k1aoc.cloudfront.net',
  // Maps.
  'cdn.leafletjs.com',
  'cdn-geoweb.s3.amazonaws.com',
  'cdn.maptiks.com',
  'api.tiles.mapbox.com',
  'd591zijq8zntj.cloudfront.net',
  // LibAnswers. RT#429697.
  '*.libanswers.com',
  // Skype. RT#429697.
  'secure.skype.com',
  // Domains required for Lightning Bolt, used by MSI. RT#563158.
  'cdn-akamai.mookie1.com',
  '*.tiqcdn.com',
  // Eyereturn, used by CBET. RT#569453.
  'o2.eyereturn.com',
  // LinkedIn. RT#580093.
  'snap.licdn.com',
  // Hootsuite Campaigns. RT#582824.
  '*.hscampaigns.com',
);
while ($site = array_shift($sites)) {
  $items[] = 'https://' . $site;
}

$img_items = array(
  '*',
  'data:',
);

echo 'default-src ' . implode(' ', $items) . '; img-src ' . implode(' ', $img_items) . "\n";
